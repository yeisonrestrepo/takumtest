<?php 
$app->get('/session', function() {
    $db = new DbHandler();
    $session = $db->getSession();
    $response["ID"] = $session['ID'];
    $response["correo"] = $session['correo'];
    $response["nombre"] = $session['nombre'];
    echoResponse(200, $session);
});

$app->post('/login', function() use ($app) {
    require_once 'passwordHash.php';
    $r = json_decode($app->request->getBody());
    verifyRequiredParams(array('correo', 'password'),$r->customer);
    $response = array();
    $db = new DbHandler();
    $password = $r->customer->password;
    $email = $r->customer->correo;
    $user = $db->getOneRecord("usuarios", "ID,nombre,correo,password,estado", array("correo" => $email));
    if ($user != NULL) {
        
        if(passwordHash::check_password($user['password'],$password)){
        $response['status'] = "success";
        $response['message'] = 'Sesi&#243;n iniciada.';
        $response['nombre'] = $user['nombre'];
        $response['ID'] = $user['ID'];
        $response['correo'] = $user['correo'];
        $response['estado'] = $user['estado'];
        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['ID'] = $user['ID'];
        $_SESSION['correo'] = $email;
        $_SESSION['nombre'] = $user['nombre'];
        } else {
            $response['status'] = "error";
            $response['message'] = 'Usuario y/o contrase&ntilde;a incorrectos';
        }
    }else {
            $response['status'] = "error";
            $response['message'] = 'El usuario no se encuentra registrado';
        }
    echoResponse(200, $response);
});

$app->get('/logout', function() {
    $db = new DbHandler();
    $session = $db->destroySession();
    $response["status"] = "info";
    $response["message"] = "Sesi&#243;n finalizada";
    echoResponse(200, $response);
});
?>