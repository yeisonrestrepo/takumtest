<?php

// Products
$app->get('/categorias', function() { 
    $db = new DbHandler();
    $rows = $db->select2("categorias c","c.ID, c.nombre, c.estado",array(), "ORDER BY (1) ASC");
    echoResponse(200, $rows);
});

$app->post('/categorias', function() use ($app) { 
    $data = json_decode($app->request->getBody());
    $mandatory = array('nombre');
    $db = new DbHandler();
    $rows = $db->insert("categorias", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product added successfully.";
    echoResponse(200, $rows);
});

$app->put('/categorias/:id', function($id) use ($app) { 
    $data = json_decode($app->request->getBody(), true);
    print_r($data);
    $condition = array('ID'=>$id);
    $mandatory = array();
    $db = new DbHandler();
    $rows = $db->update("categorias", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Informacion actualizada correctamente.";
    echoResponse(200, $rows);
});

$app->delete('/categorias/:id', function($id) { 
    $db = new DbHandler();
    $rows = $db->delete("categorias", array('ID'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Obsequio eliminado correctamente.";
    echoResponse(200, $rows);
});