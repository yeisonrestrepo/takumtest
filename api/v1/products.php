<?php

// Products
$app->get('/productos', function() { 
    $db = new DbHandler();
    $rows = $db->select2("productos p LEFT OUTER JOIN categorias c ON (p.categoria_id = c.ID)","p.ID, p.nombre, p.descripcion, p.costo, p.estado, c.nombre categoria_id",array(), "ORDER BY (1) ASC");
    echoResponse(200, $rows);
});

$app->get('/listarCategorias', function() { 
    $db = new DbHandler();
    $rows = $db->select("categorias","ID, nombre",array('estado'=> 1));
    echoResponse(200, $rows);
});

$app->post('/productos', function() use ($app) { 
    $data = json_decode($app->request->getBody());
    $mandatory = array('nombre');
    $db = new DbHandler();
    $rows = $db->insert("productos", $data, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Product added successfully.";
    echoResponse(200, $rows);
});

$app->put('/productos/:id', function($id) use ($app) { 
    $data = json_decode($app->request->getBody(), true);
    print_r($data);
    $condition = array('ID'=>$id);
    $mandatory = array();
    $db = new DbHandler();
    $rows = $db->update("productos", $data, $condition, $mandatory);
    if($rows["status"]=="success")
        $rows["message"] = "Informacion actualizada correctamente.";
    echoResponse(200, $rows);
});

$app->delete('/productos/:id', function($id) { 
    $db = new DbHandler();
    $rows = $db->delete("productos", array('ID'=>$id));
    if($rows["status"]=="success")
        $rows["message"] = "Obsequio eliminado correctamente.";
    echoResponse(200, $rows);
});