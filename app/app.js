var app = angular.module('TakumTest', ['ngRoute', 'ngAnimate', 'toaster', 'ui.bootstrap']);

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
                when('/login', {
                    title: 'Login',
                    templateUrl: 'partials/login.html',
                    controller: 'authCtrl'
                })
                .when('/logout', {
                    title: 'Logout',
                    templateUrl: 'partials/login.html',
                    controller: 'logoutCtrl'
                })
                .when('/productos', {
                    title: 'Productos',
                    templateUrl: 'partials/dashboard.html',
                    controller: 'productsCtrl'
                })
                .when('/', {
                    title: 'Login',
                    templateUrl: 'partials/login.html',
                    controller: 'authCtrl',
                    role: '0'
                })
                .when('/categorias', {
                    title: 'Categorias',
                    templateUrl: 'partials/categories.html',
                    controller: 'categoriesCtrl'
                })
                .otherwise({
                    redirectTo: '/login'
                });
    }])
        .run(function ($rootScope, $location, Data) {
            $rootScope.$on("$routeChangeStart", function (event, next, current) {
                $rootScope.authenticated = false;
                Data.get('session').then(function (results) {
                    if (results.ID) {
                        $rootScope.authenticated = true;
                        $rootScope.ID = results.ID;
                        $rootScope.nombre = results.nombre;
                        $rootScope.correo = results.correo;
                    } else {
                        var nextUrl = next.$$route.originalPath;
                        if (nextUrl == '/login') {

                        } else {
                            $location.path("/login");
                        }
                    }
                });
            });
            $rootScope.logout = function () {
                Data.get('logout').then(function (results) {
                    Data.toast(results);
                    $location.path('login');
                    return false;
                });
            }
        });