app.controller('authCtrl', function($scope, $rootScope, $routeParams, $location, $http, Data) {
    //initially set those objects to null to avoid undefined error
    $scope.login = {};
    $scope.isActive = function(viewLocation) {
        console.log(viewLocation, $location.path(), viewLocation === $location.path());
        return viewLocation === $location.path();
    };
    $scope.doLogin = function(customer) {
        Data.post('login', {
            customer: customer
        }).then(function(results) {
            Data.toast(results);
            if (results.status == "success") {
                $location.path('productos');
            }
        });
    };
    $scope.logout = function() {
        Data.get('logout').then(function(results) {
            Data.toast(results);
            $location.path('login');
        });
    }
});