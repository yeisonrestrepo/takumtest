app.controller('categoriesCtrl', function ($scope, $modal, $filter, Data) {
    $scope.categoria = {};
    Data.get('categorias').then(function (data) {
        $scope.categorias = data.data;
    });

    $scope.changeCategoryStatus = function (categoria) {
        categoria.estado = (categoria.estado == 1 ? 0 : 1);
        Data.put("categorias/" + categoria.ID, {estado: categoria.estado});
    };

    $scope.deleteCategory = function (categoria) {
        if (confirm("\xBFEst\xE1 seguro que desea eliminar el obsequio?")) {
            Data.delete("categorias/" + categoria.ID).then(function (result) {
                $scope.categorias = _.without($scope.categorias, _.findWhere($scope.categorias, {ID: categoria.ID}));
            });
        }
    };

    $scope.open = function (p, size) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/categoryEdit.html',
            controller: 'categoryEditCtrl',
            size: size,
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

        modalInstance.result.then(function (selectedObject) {
            Data.get('categorias').then(function (data) {
                $scope.categorias = data.data;
            });
        });
    };

    $scope.columns = [
        {text: "ID", predicate: "ID", sortable: true, dataType: "number"},
        {text: "Nombre", predicate: "nombre", sortable: true},
        {text: "Estado", predicate: "estado", sortable: true},
        {text: "Acci\xF3n", predicate: "", sortable: false}
    ];

});


app.controller('categoryEditCtrl', function ($scope, $modalInstance, item, Data) {

    $scope.categoria = angular.copy(item);
    $scope.categorias = {};

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };
    
    $scope.buttonText = (item.ID > 0) ? 'Actualizar categoria' : 'Agregar nueva categoria';

    var original = item;
    $scope.isClean = function () {
        return angular.equals(original, $scope.categoria);
    }

    $scope.saveCategory = function (categoria) {
        if (categoria.ID > 0) {
            Data.put('categorias/' + categoria.ID, categoria).then(function (result) {
                if (result.status != 'error') {
                    var x = angular.copy(categoria);
                    x.save = 'update';
                    $modalInstance.close(x);
                } else {
                    console.log(result);
                }
            });
        } else {
            Data.post('categorias', categoria).then(function (result) {
                if (result.status != 'error') {
                    var x = angular.copy(categoria);
                    x.save = 'insert';
                    x.id = result.data;
                    $modalInstance.close(x);
                } else {
                    console.log(result);
                }
            });
        }
    };
});
