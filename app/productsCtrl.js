app.controller('productsCtrl', function ($scope, $modal, $filter, Data) {
    $scope.producto = {};
    Data.get('productos').then(function (data) {
        $scope.productos = data.data;
    });

    $scope.changeProductStatus = function (producto) {
        producto.estado = (producto.estado == 1 ? 0 : 1);
        Data.put("productos/" + producto.ID, {estado: producto.estado});
    };

    $scope.deleteProduct = function (producto) {
        if (confirm("\xBFEst\xE1 seguro que desea eliminar el obsequio?")) {
            Data.delete("productos/" + producto.ID).then(function (result) {
                $scope.productos = _.without($scope.productos, _.findWhere($scope.productos, {ID: producto.ID}));
            });
        }
    };

    $scope.open = function (p, size) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/productEdit.html',
            controller: 'productEditCtrl',
            size: size,
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

        modalInstance.result.then(function (selectedObject) {
            Data.get('productos').then(function (data) {
                $scope.productos = data.data;
            });
        });
    };

    $scope.columns = [
        {text: "ID", predicate: "ID", sortable: true, dataType: "number"},
        {text: "Nombre", predicate: "nombre", sortable: true},
        {text: "Precio", predicate: "costo", sortable: true, dataType: "number"},
        {text: "Descripci\xF3n", predicate: "descripcion", sortable: true},
        {text: "Categor\xEDa", predicate: "categoria_id", sortable: true},
        {text: "Estado", predicate: "estado", sortable: true},
        {text: "Acci\xF3n", predicate: "", sortable: false}
    ];

});


app.controller('productEditCtrl', function ($scope, $modalInstance, item, Data) {

    $scope.producto = angular.copy(item);
    $scope.categorias = {};

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };
    
    $scope.buttonText = (item.ID > 0) ? 'Actualizar Obsequio' : 'Agregar nuevo obsequio';

    Data.get('listarCategorias').then(function (data) {
        $scope.categorias = data.data;
    });

    var original = item;
    $scope.isClean = function () {
        return angular.equals(original, $scope.producto);
    }

    $scope.saveProduct = function (producto) {
        if (producto.ID > 0) {
            Data.put('productos/' + producto.ID, producto).then(function (result) {
                if (result.status != 'error') {
                    var x = angular.copy(producto);
                    x.save = 'update';
                    $modalInstance.close(x);
                } else {
                    console.log(result);
                }
            });
        } else {
            Data.post('productos', producto).then(function (result) {
                if (result.status != 'error') {
                    var x = angular.copy(producto);
                    x.save = 'insert';
                    x.id = result.data;
                    $modalInstance.close(x);
                } else {
                    console.log(result);
                }
            });
        }
    };
});
