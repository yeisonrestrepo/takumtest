<!DOCTYPE html>
<html lang="en" ng-app="TakumTest">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>TakumTest</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="css/custom.css" type="text/css" />
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
        <link href="css/toaster.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <style>
            a {
                color: #fff;
            }
        </style>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]><link href= "css/bootstrap-theme.css"rel= "stylesheet" >

<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    </head>

    <body>
        <div ng-cloak="">
            <div class="navbar navbar-default navbar-fixed-top" role="navigation" ng-show='authenticated'>
                <div class="container"> 
                    <ul class="nav navbar-nav">
                        <li><a href="#/productos">Productos</a></li>     
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Parametrizar
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#/categorias">Categor&iacute;as</a></li>
                                <li><a href="#/usuarios">Usuarios</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-user"></span> 
                                <strong>{{nombre}}</strong>
                                <span class="glyphicon glyphicon-chevron-down"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="navbar-login">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <p class="text-center">
                                                    <span class="glyphicon glyphicon-user icon-size"></span>
                                                </p>
                                            </div>
                                            <div class="col-lg-8">
                                                <p class="text-left"><strong>{{nombre}}</strong></p>
                                                <p class="text-left small">{{correo}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="navbar-login navbar-login-session">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p>
                                                    <a id="logout" ng-click="logout();" class="btn btn-danger btn-block">Cerrar sesi&oacute;n</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="container" style="margin-top:20px;">

                <div data-ng-view="" id="ng-view" class="slide-animation"></div>

            </div>
    </body>
    <toaster-container toaster-options="{'time-out': 3000}"></toaster-container>

    <!-- Lib -->
    <script src="js/angular.min.js"></script>
    <script src="js/ui-bootstrap-tpls-0.11.2.min.js"></script>
    <script src="js/angular-route.min.js"></script>
    <script src="js/angular-animate.min.js"></script>
    <script src="js/toaster.js"></script>

    <!-- AngularJS custom codes -->
    <script src="app/app.js"></script>
    <script src="app/data.js"></script>
    <script src="app/directives.js"></script>
    <script src="app/authCtrl.js"></script>
    <script src="app/productsCtrl.js"></script>
    <script src="app/categoriesCtrl.js"></script>

    <!-- Some Bootstrap Helper Libraries -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/underscore.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <script src="js/app.js"></script>
</html>

